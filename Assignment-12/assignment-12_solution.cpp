/*
@auther	Hyung-Gyu Ryoo
@email	hgryoo@pnu.edu
@description	assignment 12 solution code
*/
#include <iostream>
#include <set>
#include <algorithm>

#include <cstdlib>
#include <cmath>

#define PI	3.14159265358979323846

using namespace std;

class Point;
class Shape {
private:
	virtual bool isEqual(const Shape& other) const = 0;
protected:
	int type; // POINT, CIRCLE, RECTANGLE, or TRIANGLE
public:
	int getType() const {
		return type;
	}
	virtual double area() = 0;
	virtual bool isContaining(const Point&) = 0;
	enum { POINT, CIRCLE, RECTANGLE, TRIANGLE };
	friend bool operator==(const Shape &a, const Shape &b);

	virtual Shape* clone() = 0;
};

bool operator==(const Shape &a, const Shape &b) {
	return (a.getType() == b.getType()) && a.isEqual(b);
}

class Point : public Shape {
private:
	double x, y;
	bool isEqual(const Shape& other) const {
		bool result = false;

		Point *p = (Point *)& other;
		if (x == p->x && y == p->y) {
			result = true;
		}

		return result;
	}
public:
	Point(double _x, double _y) : x(_x), y(_y) {
		this->type = POINT;
	}
	double getX() const {
		return x;
	}
	double getY() const {
		return y;
	}
	virtual double area() {
		return 0;
	}
	virtual bool isContaining(const Point& p) {
		bool result = false;
		if (*this == p) {
			result = true;
		}
		return result;
	}
	virtual Point* clone() {
		return new Point(*this);
	}
};

class Rectangle : public Shape {
private:
	Point rightUpper, leftLower;
	virtual bool isEqual(const Shape& other) const {
		bool result = false;
		if (type == other.getType()) {
			const Rectangle& r = dynamic_cast<const Rectangle&>(other);
			if (leftLower == r.leftLower && rightUpper == r.rightUpper) {
				result = true;
			}
		}
		return result;
	}
public:
	Rectangle(const Point& lower, const Point& upper) :leftLower(lower), rightUpper(upper) {
		this->type = RECTANGLE;
	}
	virtual double area() {
		double minX = leftLower.getX();  double minY = leftLower.getY();
		double maxX = rightUpper.getX(); double maxY = rightUpper.getY();

		return (maxX - minX) * (maxY - minY);
	}
	virtual bool isContaining(const Point& p) {
		bool result = false;

		double minX = leftLower.getX();  double minY = leftLower.getY();
		double maxX = rightUpper.getX(); double maxY = rightUpper.getY();

		double px = p.getX(); double py = p.getY();

		if ((px >= minX && px <= maxX) && (py >= minY && py <= maxY)) {
			result = true;
		}

		return result;
	}
	virtual Rectangle* clone() {
		return new Rectangle(*this);
	}
};

class Circle : public Shape {
private:
	Point center;
	double radius;

	virtual bool isEqual(const Shape& other) const {
		bool result = false;
		if (type == other.getType()) {
			const Circle* r = (Circle*)&other;
			if ((center == r->getCenter()) && (radius == r->getRadius())) {
				return true;
			}
		}
		return result;
	}
public:
	Circle(const Point& c, double r) : center(c), radius(r) {
		this->type = CIRCLE;
	}
	Point getCenter() const {
		return center;
	}
	double getRadius() const {
		return radius;
	}
	virtual double area() {
		return PI * radius * radius;
	}
	virtual bool isContaining(const Point& p) {
		bool result = false;

		double centerX = center.getX(); double centerY = center.getY();
		double px = p.getX(); double py = p.getY();

		if (((centerX - px) * (centerX - px) + (centerY - py) * (centerY - py)) <= radius * radius) {
			result = true;
		}

		return result;
	}
	virtual Circle* clone() {
		return new Circle(*this);
	}
};

class Triangle : public Shape {
private:
	Point p1, p2, p3;
	virtual bool isEqual(const Shape& other) const {
		bool result = false;
		if (type == other.getType()) {
			const Triangle* r = (Triangle*)&other;
			if (p1 == r->p1 && p2 == r->p2 && p3 == r->p3) {
				return true;
			}
		}
		return result;
	}
	double sign(Point p1, Point p2, Point p3) {
		return (p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY());
	}
public:
	Triangle(const Point& _p1, const Point& _p2, const Point& _p3) : p1(_p1), p2(_p2), p3(_p3) {
		//TODO : you should check these three point are not in the same straight line. 
		this->type = TRIANGLE;
	}
	virtual double area() {
		double x1 = p1.getX(); double x2 = p2.getX(); double x3 = p3.getX();
		double y1 = p1.getY(); double y2 = p2.getY(); double y3 = p3.getY();

		return abs(((x1 * y2) + (x2 * y3) + (x3 * y1) - (x2 * y1) - (x3 * y2) - (x1 * y3)) / 2);
	}
	virtual bool isContaining(const Point& p) {
		bool b1 = sign(p, p1, p2) < 0.0; bool b2 = sign(p, p2, p3) < 0.0; bool b3 = sign(p, p3, p1) < 0.0;
		return ((b1 == b2) && (b2 == b3));
	}
	virtual Triangle* clone() {
		return new Triangle(*this);
	}
};

class ShapeSet {
private:
	set<Shape*>	shapes;
public:
	ShapeSet() {}
	~ShapeSet() {
		set<Shape*>::iterator it = shapes.begin();
		while (it != shapes.end()) {
			delete *it;
			++it;
		}
	}
	ShapeSet(const ShapeSet& copy) {
		set<Shape*>::iterator it = copy.shapes.begin();
		for (it = copy.shapes.begin(); it != copy.shapes.end(); ++it) {
			Shape* shapePtr = *it;
			shapes.insert(shapePtr->clone());
		}
	}

	int getNumShapes() const {
		return shapes.size();
	}

	ShapeSet& operator+(Shape& s) { //add a shape to ShapeSet. Don’t allow duplication.
		bool duplicated = false;
		set<Shape*>::iterator it = shapes.begin();
		for (it = shapes.begin(); it != shapes.end(); ++it) {
			Shape* shapePtr = *it;
			if (*shapePtr == s) { // duplicated
				duplicated = true;
				break;
			}
		}

		if (!duplicated) {
			Shape* newShape = s.clone();
			shapes.insert(newShape);
		}

		return *this;
	}
	ShapeSet& operator-(const Shape& s) { // remove s from ShapeSet if there is a same shape
		set<Shape*>::iterator it = shapes.begin();
		for (it = shapes.begin(); it != shapes.end(); ++it) {
			Shape* shapePtr = *it;
			if (*shapePtr == s) { // duplicated
				shapes.erase(it);
				delete shapePtr;
				break; // if duplicated Shape is more than 2, you can remove it.
			}
		}
		return *this;
	}
	ShapeSet& operator=(const ShapeSet& s) { // copy ShpaeSet if parameter is not same
		if (this != &s) {
		    shapes.clear();
			set<Shape*> oldShape = s.shapes;
			set<Shape*>::iterator it = oldShape.begin();
			for (it = oldShape.begin(); it != oldShape.end(); ++it) {
				Shape* shapePtr = *it;
				shapes.insert(shapePtr->clone());
			}
		}
		return *this;
	}
	ShapeSet search(const Point& p) { // return shapes that contains point p
		ShapeSet searchedSet;

		set<Shape*>::iterator it = shapes.begin();
		for (it = shapes.begin(); it != shapes.end(); ++it) {
			Shape* shapePtr = *it;
			if (shapePtr->isContaining(p) == true) { // containing shape
				searchedSet = searchedSet + *shapePtr;
			}
		}
		return searchedSet;
	}
	double totalArea() { // total area of shapes in the set
		double result = 0;
		set<Shape*>::iterator it = shapes.begin();
		for (it = shapes.begin(); it != shapes.end(); ++it) {
			Shape* shapePtr = *it;
			result += shapePtr->area();
		}
		return result;
	}
};

int main() {
	ShapeSet* ss = new ShapeSet();
	ShapeSet& shapeSet = *ss;

	// Rectangle
	for (int i = 0; i < 5; i++) {
		Point leftLower(i * 2, 0);
		Point rightUpper(i * 2 + 2, 2);
		Rectangle rectangle(leftLower, rightUpper);

		shapeSet = shapeSet + rectangle;
	}

	cout << "Total area : " << shapeSet.totalArea() << endl;	// Total area : 20~

	// Circle
	for (int i = 0; i < 5; i++) {
		Point center(i * 2 + 1, 3);
		double radius = 1;
		Circle circle(center, radius);

		shapeSet = shapeSet + circle;
	}

	cout << "Total area : " << shapeSet.totalArea() << endl;	// Total area : 35.7~

	// Traingle
	for (int i = 0; i < 5; i++) {
		Point triangleP1(i * 2, 4);
		Point triangleP2(i * 2 + 1, 6);
		Point triangleP3(i * 2 + 2, 4);
		Triangle triangle(triangleP1, triangleP2, triangleP3);

		shapeSet = shapeSet + triangle;
	}

	cout << "Total area : " << shapeSet.totalArea() << endl; 	// Total area : 45.7~

	// duplicated Rectangle
	Point rectP1(0, 0);
	Point rectP2(2, 2);
	Rectangle rect(rectP1, rectP2);

	ShapeSet newShapeSet;
	newShapeSet = shapeSet + rect;
	cout << "Total area : " << newShapeSet.totalArea() << endl; // Total area : 45.7~

	//delete ss, if operator= is not implemented, following statements occur error.
	delete ss;
	newShapeSet = newShapeSet - rect;
	cout << "Total area : " << newShapeSet.totalArea() << endl; // Total area : 41.7~

	// add overlapped Rectangle, Circle and Triangle
	Point center(1, 1);
	Circle circle(center, 1);

	Point p1(0, 0);
	Point p2(1, 2);
	Point p3(2, 0);
	Triangle triangle(p1, p2, p3);

	newShapeSet = newShapeSet + rect;
	newShapeSet = newShapeSet + circle;
	newShapeSet = newShapeSet + triangle;

	// Search
	for (int i = 0; i < 3; i++) {
		// (1,1), (3,3), (5,5)
		Point searchPoint(i * 2 + 1, i * 2 + 1);

		// (4 + PI + 2), PI, 2
		ShapeSet result = newShapeSet.search(searchPoint);

		cout << result.totalArea() << endl;
	}

	system("pause");
	/*
	한
	학
	기
	동
	안
	수
	고
	하
	셨
	습
	니
	다
	*/
	return EXIT_SUCCESS;
}