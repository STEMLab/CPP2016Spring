int main() {
	ShapeSet* ss = new ShapeSet();
	ShapeSet& shapeSet = *ss;

	// Rectangle
	for (int i = 0; i < 5; i++) {
		Point leftLower(i * 2, 0);
		Point rightUpper(i * 2 + 2, 2);
		Rectangle rectangle(leftLower, rightUpper);

		shapeSet = shapeSet + rectangle;
	}

	cout << "Total area : " << shapeSet.totalArea() << endl;	// Total area : 20~

	// Circle
	for (int i = 0; i < 5; i++) {
		Point center(i * 2 + 1, 3);
		double radius = 1;
		Circle circle(center, radius);

		shapeSet = shapeSet + circle;
	}

	cout << "Total area : " << shapeSet.totalArea() << endl;	// Total area : 35.7~

	// Traingle
	for (int i = 0; i < 5; i++) {
		Point triangleP1(i * 2, 4);
		Point triangleP2(i * 2 + 1, 6);
		Point triangleP3(i * 2 + 2, 4);
		Triangle triangle(triangleP1, triangleP2, triangleP3);

		shapeSet = shapeSet + triangle;
	}

	cout << "Total area : " << shapeSet.totalArea() << endl; 	// Total area : 45.7~

	// duplicated Rectangle
	Point rectP1(0, 0);
	Point rectP2(2, 2);
	Rectangle rect(rectP1, rectP2);

	ShapeSet newShapeSet;
	newShapeSet = shapeSet + rect;
	cout << "Total area : " << newShapeSet.totalArea() << endl; // Total area : 45.7~

	//delete ss, if operator= is not implemented, following statements occur error.
	delete ss;
	newShapeSet = newShapeSet - rect;
	cout << "Total area : " << newShapeSet.totalArea() << endl; // Total area : 41.7~

	// add overlapped Rectangle, Circle and Triangle
	Point center(1, 1);
	Circle circle(center, 1);

	Point p1(0, 0);
	Point p2(1, 2);
	Point p3(2, 0);
	Triangle triangle(p1, p2, p3);

	newShapeSet = newShapeSet + rect;
	newShapeSet = newShapeSet + circle;
	newShapeSet = newShapeSet + triangle;

	// Search
	for (int i = 0; i < 3; i++) {
		// (1,1), (3,3), (5,5)
		Point searchPoint(i * 2 + 1, i * 2 + 1);

		// 4, PI, 2
		Shape* result = newShapeSet.search(searchPoint);
		if (result== NULL) {
			cout << "NULL" << endl;
		}
		else {
			cout << result->area() << endl;
		}

		/*
		// (4 + PI + 2), PI, 2
		ShapeSet result = newShapeSet.search(searchPoint);

		cout << result.totalArea() << endl;
		*/
	}

	return EXIT_SUCCESS;
}
