#include <iostream>
#include <cstdlib>

using namespace std;

class IntegerSet {
	int numElements, maxNumElements;
	int *elements;
public:
	IntegerSet(int max) {
		numElements = 0;
		maxNumElements = max;
		elements = new int[max];
	}
	~IntegerSet();
	int addAnElement(int); // add an integer element, to implement
	IntegerSet operator+(const IntegerSet&); // union, to implement
	IntegerSet operator-(const IntegerSet&); // difference, to implement
	IntegerSet operator*(const IntegerSet&); // intersection, to implement
	int operator~();
	friend const ostream& operator<<(const ostream&, const IntegerSet&); // given as below
};

const ostream& operator<<(const ostream& osIntegerSet, const IntegerSet& integerSet) {
	cout << "number of elements : " << integerSet.numElements << endl;
	for (int i = 0; i < integerSet.numElements; i++) {
		cout << integerSet.elements[i] << " ";
	}
	cout << endl;

	return osIntegerSet;
}

int main() {	
	int values[5] = { 1, 2, 3, 4, 5 };
	int values2[5] = { 4, 5, 6, 7, 8 };
	IntegerSet is1(5);
	IntegerSet is2(5);
	
	for (int i = 0; i < 5; i++) {
		is1.addAnElement(values[i]);
	}

	for (int i = 0; i < 5; i++) {
		is2.addAnElement(values2[i]);
	}	
	
	IntegerSet unionSet = is1 + is2;
	cout << "Union Set : is1 + is2" << endl;
	cout << unionSet;

	IntegerSet diffSet1 = is1 - is2;
	cout << "Difference Set : is1 - is2" << endl;
	cout << diffSet1;

	IntegerSet diffSet2 = is2 - is1;
	cout << "Difference Set : is2 - is1" << endl;
	cout << diffSet2;

	IntegerSet intersectionSet = is1 * is2;
	cout << "Intersection Set : is1 * is2" << endl;
	cout << intersectionSet;

	IntegerSet unionSet2 = diffSet1 + diffSet2 + intersectionSet;
	cout << "Union Set : diffSet1 + diffSet2 + intersectionSet " << endl;
	cout << unionSet2;

	int max = ~unionSet2;
	cout << "max value : " << max << endl;

	return EXIT_SUCCESS;
}

