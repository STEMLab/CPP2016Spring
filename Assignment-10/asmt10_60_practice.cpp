#include <iostream>

using namespace std;

class RectangleSet {
	int numElements, maxNumElements;
	Rectangle *elements;

public:
	RectangleSet(int max) {
		numElements = 0;
		maxNumElements = max;
		elements = new Rectangle[max];
	}
	~RectangleSet();
	int addAnElement(Rectangle);
	RectangleSet operator*(const Line& line);
	friend ostream& operator<<(ostream&, const RectangleSet&);
};

ostream& operator<<(ostream& os, const RectangleSet& rectangleSet) {
	cout << "[ RectangleSet ] => number of elements: " << rectangleSet.numElements << endl;
	for (int i = 0; i < rectangleSet.numElements; i++) {
		cout << "\t" << rectangleSet.elements[i];
	}
	cout << endl;
	return os;
}

int main() {
	RectangleSet rectangleSet(10);
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 5; j++) {
			Point lower(j, i);
			Point upper(j + 1, i + 1);
			Rectangle rectangle(lower, upper);

			rectangleSet.addAnElement(rectangle);
		}
	}
	cout << rectangleSet;

	Point p1(-1, -0.5);
	Point p2(6, 0.5);
	Line line1(p1, p2);
	RectangleSet intersectionSet1 = rectangleSet * line1;
	cout << "IntersectionSet1 :" << endl;
	cout << intersectionSet1;

	Point p3(-1, 1.5);
	Point p4(6, 1.5);
	Line line2(p3, p4);
	RectangleSet intersectionSet2 = rectangleSet * line2;
	cout << "IntersectionSet2 : " << endl;
	cout << intersectionSet2;

	return EXIT_SUCCESS;
}
