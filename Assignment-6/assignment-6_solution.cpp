/*
*author soojin Kim  
*/
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

class Student;

class Department {
	char* name;
	int maxStudents, numStudents;
	Student *students;

public:
	Department(char *name, int maxStudents);
	~Department();
	void AddAStudent(Student *);
	void sortStudents(bool ascend);
	Student findStudent(char* name);
	void PrintDepartment();
};

class Student {
	char* name;
	int year;
	int ID;

public:
	Student();
	Student(char* _name, int _year, int _ID);
	char* getName();
	int getYear();
	int getID();
};

Student::Student() {
	name = NULL;
	year = 0;
	ID = 0;
}

Student::Student(char* _name, int _year, int _ID) {
	int len = strlen(_name);

	name = new char[len + 1];
	strcpy(name, _name);

	year = _year;

	ID = _ID;
}

char* Student::getName() {
	return name;
}

int Student::getYear() {
	return year;
}

int Student::getID() {
	return ID;
}

Department::Department(char* _name, int _Maxstudents) {
	int len = strlen(_name);
	name = new char[len + 1];
	strcpy(name, _name);

	maxStudents = _Maxstudents;
	numStudents = 0;

	students = new Student[_Maxstudents];
} //initialize department name, maxStudents, numStudents and make space for students up to maxStudents

void Department::AddAStudent(Student* s) {
	if (numStudents >= maxStudents) {
		cout << "error_numStudent_over_maxStudent" << endl;
		exit(1);
	}
	else {
		Student temp(s->getName(), s->getYear(), s->getID());
		students[numStudents] = temp;
		numStudents++;
	}
} //add Student to Student array in Department. but print error message for numStudents of maxStudents and over, 

void Department::PrintDepartment() {
	cout << "Department name : " << name << endl;
	for (int i = 1; i < numStudents + 1; i++) {
		cout << "Department's " << i << "th name: " << (students + i - 1)->getName() << endl;
	}
} //print Department_name and every Department_Student_name 

void Department::sortStudents(bool ascend) {
	Student temp;

	if (ascend == true) {
		for (int i = 0; i < numStudents; i++) {
			for (int j = i + 1; j < numStudents; j++) {
				if (strcmp((students + i)->getName(), (students + j)->getName()) > 0) {
					temp = *(students + i);
					*(students + i) = *(students + j);
					*(students + j) = temp;
				}
			}
		}
	}


	else if (ascend == false) {
		for (int i = 0; i < numStudents; i++) {
			for (int j = i + 1; j < numStudents; j++) {
				if (strcmp((students + i)->getName(), (students + j)->getName()) < 0) {
					temp = *(students + i);
					*(students + i) = *(students + j);
					*(students + j) = temp;
				}
			}
		}
	}
} // sort Student array in ascending order if ascend is true or decending order if ascend is false.

Student Department::findStudent(char* name)
{
	int i; 
	int j = 0;
	Student s("Not Found", 0, 0);
	for (i = 0; i < numStudents; i++) {
		if (strcmp(name, (students + i)->getName()) == 0) {
			s = students[i];
		}
	}
	return s;
}//find Student in student array using Student name.

Department :: ~Department()
{
	delete name;
	delete[] students;
}

int main() {
	Department cse("CSE", 10);

	Student stud_maldong("Maldong", 23, 201322332);
	Student stud_jinsoo("Jinsoo", 21, 201599999);
	Student stud_soomin("Soomin", 24, 201211231);

	cse.AddAStudent(&stud_maldong);
	cse.AddAStudent(&stud_soomin);
	cse.AddAStudent(&stud_jinsoo);
	cse.sortStudents(false);
	cse.PrintDepartment();

	Student student = cse.findStudent("Jinsoo");
	if (strcmp(student.getName(),"Not Found") == 0) {
		cout << "NOT FOUND" << endl;
	}
	else {
		cout << student.getName() << " " << student.getYear() << " " << student.getID() << endl;
	}

	return 0;
}